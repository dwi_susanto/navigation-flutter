import 'package:flutter/material.dart';

class TabHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Center(
        child: new Column(
          children: <Widget>[
            new Padding(padding: new EdgeInsets.all(20.0),),
            new Text("Home", style: TextStyle(fontSize: 20.0),),
            new Padding(padding: new EdgeInsets.all(20.0),),
            new Image.asset("img/home.png", width: 50.0,),
            new Padding(padding: new EdgeInsets.all(20.0),),
            new Image(image: NetworkImage("https://www.theaa.ie/blog/wp-content/uploads/2013/10/Home-Pic.jpg"),width: 300.0,)
          ],
        ),
      ),
    );
  }
}