import 'package:flutter/material.dart';

class TabCategory extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Center(
        child: new Column(
          children: <Widget>[
            new Padding(padding: new EdgeInsets.all(20.0),),
            new Text("Category", style: new TextStyle(fontSize: 20.0),),
            new Padding(padding: new EdgeInsets.all(20.0),),
            new Image.asset("img/category.png", width: 50.0,),
            new Padding(padding: new EdgeInsets.all(20.0),),
            new Image(image: NetworkImage("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQh0nLbOEkdEMbDUV36ItqY1kp4mOkKMOPyaoYR3boasBPoEUZU"),width: 300.0,)
          ],
        ),
      ),
    );
  }
}