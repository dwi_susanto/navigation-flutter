import 'package:flutter/material.dart';

class TabProfile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Container(
      child: Center(
        child: Column(
          children: <Widget>[
            new Padding(padding: new EdgeInsets.all(20.0)),
            new Text("Profile", style: TextStyle(fontSize: 20.0),),
            new Padding(padding: new EdgeInsets.all(20.0),),
            new Image.asset("img/profile.png", width: 50.0,),
            new Padding(padding: new EdgeInsets.all(20.0),),
            new Image(image: NetworkImage("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRA0XOQ5aZIK2eE0WX7gsS2ncFwNHd1y12ZaK5By0GVYI5-MGaO"),width: 300.0,)
          ],
        ),
      ),
    );
  }
}