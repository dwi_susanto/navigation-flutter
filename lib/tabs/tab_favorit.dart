import 'package:flutter/material.dart';

class TabFavorit extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Container(
      child: Center(
        child: Column(
          children: <Widget>[
            new Padding(padding: EdgeInsets.all(20.0),),
            new Text("Favorit", style: new TextStyle(fontSize: 20.0),),
            new Padding(padding: EdgeInsets.all(20.0),),
            new Image.asset("img/favorit.png", width: 50.0,),
            new Padding(padding: new EdgeInsets.all(20.0),),
            new Image(image: NetworkImage("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSAB1DhPqq4wzT29RYvfQwuSy8Arct1esWGUt_4Xlt5KuZvfZokVA"),width: 300.0,)
          ],
        ),
      ),
    );
  }
}