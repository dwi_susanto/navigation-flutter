import 'package:flutter/material.dart';
import './tabs/tab_home.dart' as tab_home;
import './tabs/tab_category.dart' as tab_category;
import './tabs/tab_favorit.dart' as tab_favorit;
import './tabs/tab_profile.dart' as tab_profile;

void main() {
  runApp(new MaterialApp(
    title: "Apps",
    home: new Home(),
  ));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home>  with SingleTickerProviderStateMixin{

  TabController controller;

  @override
    void initState() {
      //controller
      controller = new TabController(vsync: this, length: 4);
      super.initState();
    }

  @override
    void dispose() {
      controller.dispose();
      super.dispose();
    }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
    appBar: new AppBar(
      backgroundColor: Colors.blueAccent,
          title: new Text("News"),

          //top navigator
          bottom: new TabBar(
            controller: controller,
            tabs: <Widget>[
              new Tab(icon: new Icon(Icons.home),text: "Home",),
              new Tab(icon: new Icon(Icons.category), text: "Category",),
              new Tab(icon: new Icon(Icons.favorite), text: "Favorit",),
              new Tab(icon: new Icon(Icons.verified_user), text: "Profile",)
            ],
          ),
          ),

          //body content
          body: new TabBarView(
            controller: controller,
            children: <Widget>[
              new tab_home.TabHome(),
              new tab_category.TabCategory(),
              new tab_favorit.TabFavorit(),
              new tab_profile.TabProfile()
            ],
          ),

          //bottom navigator
          bottomNavigationBar: new Material(
            color: Colors.blueAccent,
            child: new TabBar(
              controller: controller,
              tabs: <Widget>[
              new Tab(icon: new Icon(Icons.home),),
              new Tab(icon: new Icon(Icons.category),),
              new Tab(icon: new Icon(Icons.favorite),),
              new Tab(icon: new Icon(Icons.verified_user),)
              ],
            ),
          ),

        );
  }
}